# Tuna de la EAN website

This project is the source code for the tuna de la EAN webpage

## Deploy Status for branches
Master: 
[![Netlify Status](https://api.netlify.com/api/v1/badges/6d498972-2d6d-4cfb-9591-28c7cc06dc2b/deploy-status?branch=master)](https://app.netlify.com/sites/tunadelaean/deploys)
Develop: 
[![Netlify Status](https://api.netlify.com/api/v1/badges/6d498972-2d6d-4cfb-9591-28c7cc06dc2b/deploy-status?branch=develop)](https://app.netlify.com/sites/tunadelaean/deploys)

## Dependencies

```
npm
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
