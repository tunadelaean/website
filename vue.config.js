// vue.config.js
const { defineConfig } = require('@vue/cli-service')

module.exports = defineConfig({
  transpileDependencies: [],
  pluginOptions: {
    sitemap: {
      baseURL: 'https://tunadelaean.com',
      routes: [
        {
          path: '/',
          changefreq: 'daily',
          priority: 1.0
        },
        {
          path: '/about',
          changefreq: 'weekly',
          priority: 0.8
        },
        {
          path: '/contact',
          changefreq: 'weekly',
          priority: 0.8
        },
        {
          path: '/services',
          changefreq: 'weekly',
          priority: 0.8
        },
        {
          path: '/listenus',
          changefreq: 'weekly',
          priority: 0.8
        }
      ]
    }
  }
})
